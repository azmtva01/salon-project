from django.contrib import admin
from .models import Lady, Category, Portfolio, Hair, Cosmet, Makeup, Nail

admin.site.register(Lady)
admin.site.register(Category)
admin.site.register(Portfolio)
admin.site.register(Hair)
admin.site.register(Cosmet)
admin.site.register(Makeup)
admin.site.register(Nail)
