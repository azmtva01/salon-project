from django.urls import path
from . import views

urlpatterns = [
    path('', views.category, name='lady_list'),
    path('create/lady', views.lady_create, name='lady_create'),
    path('<int:pk>/update/', views.lady_update, name='lady_update'),
    path('<int:pk>/delete/', views.lady_delete, name='lady_delete'),

    path('salon/', views.salon, name='salon'), 

    path('portfolio/', views.portfolio, name='portfolio'),
    path('create/portfolio', views.portfolio_create, name='portfolio_create'),
    path('<int:pk>/update/portfolio', views.portfolio_update, name='portfolio_update'),
    path('<int:pk>/delete/portfolio', views.portfolio_delete, name='portfolio_delete'),


    path('hair/', views.hair, name='hair'),
    path('create/hair', views.hair_create, name='hair_create'),
    path('<int:pk>/update/hair', views.hair_update, name='hair_update'),
    path('<int:pk>/delete/hair', views.hair_delete, name='hair_delete'),


    path('makeup/', views.makeup, name='makeup'),
    path('create/makeup', views.makeup_create, name='makeup_create'),
    path('<int:pk>/update/makeup', views.makeup_update, name='makeup_update'),
    path('<int:pk>/delete/makeup', views.makeup_delete, name='makeup_delete'),
    
    path('cosmet/', views.cosmet, name='cosmet'),
    path('create/cosmet', views.cosmet_create, name='cosmet_create'),
    path('<int:pk>/update/cosmet', views.cosmet_update, name='cosmet_update'),
    path('<int:pk>/delete/cosmet', views.cosmet_delete, name='cosmet_delete'),

    path('nail/', views.nail, name='nail'),
    path('create/nail', views.nail_create, name='nail_create'),
    path('<int:pk>/update/nail', views.nail_update, name='nail_update'),
    path('<int:pk>/delete/nail', views.nail_delete, name='nail_delete'),
]

