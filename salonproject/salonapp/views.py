from django.shortcuts import render, redirect, get_object_or_404
from .models import Lady, Category, Portfolio, Hair, Cosmet, Makeup, Nail
from .forms import LadyForm, PortfolioForm, HairForm, CosmetForm, MakeupForm, NailForm


def category(request):
    categories = Category.objects.all
    return render(request, 'salonapp/lady_list.html', {'categories':categories})


def salon(request):
    salons = Lady.objects.all
    return render(request, 'salonapp/salon.html', {'salons':salons})


def portfolio(request):
    portfolios = Portfolio.objects.all
    return render(request, 'salonapp/portfolio.html', {'portfolios': portfolios})


def portfolio_create(request):
    if request.method == "POST":
        form = PortfolioForm(request.POST, request.FILES)
        if form.is_valid():
            portfolio = form.save(commit=False)
            portfolio.save()
            return redirect('portfolio')
    else:
        form = PortfolioForm()
    return render(request, 'salonapp/portfolio_create.html', {'form': form})


def portfolio_update(request, pk):
    portfolio = get_object_or_404(Portfolio, pk=pk)
    if request.method == "POST":
        form = PortfolioForm(request.POST, request.FILES,  instance=portfolio)
        if form.is_valid():
            portfolio = form.save(commit=False)
            portfolio.save()
            return redirect('portfolio')
    else:
        form = PortfolioForm(instance=portfolio)
    return render(request, 'salonapp/portfolio_update.html', {'form': form})


def portfolio_delete(request, pk):
    salonapp = get_object_or_404(Portfolio, pk=pk)
    salonapp.delete()
    return redirect('portfolio')


def lady_create(request):
    if request.method == "POST":
        form = LadyForm(request.POST, request.FILES)
        if form.is_valid():
            lady = form.save(commit=False)
            lady.save()
            return redirect('lady')
    else:
        form = LadyForm()
    return render(request, 'salonapp/lady_create.html', {'form': form})


def lady_update(request, pk):
    lady = get_object_or_404(Lady, pk=pk)
    if request.method == "POST":
        form = LadyForm(request.POST, request.FILES,  instance=lady)
        if form.is_valid():
            lady = form.save(commit=False)
            lady.save()
            return redirect('lady')
    else:
        form = LadyForm(instance=lady)
    return render(request, 'salonapp/lady_update.html', {'form': form})


def lady_delete(request, pk):
    salonapp = get_object_or_404(Lady, pk=pk)
    salonapp.delete()
    return redirect('lady')


def hair(request):
    hairs = Hair.objects.all() 
    return render(request, 'salonapp/hair.html', {'hairs':hairs})


def hair_create(request):
    if request.method == "POST":
        form = HairForm(request.POST, request.FILES)
        if form.is_valid():
            hair = form.save(commit=False)
            hair.save()
            return redirect('hair')
    else:
        form = HairForm()
    return render(request, 'salonapp/hair_create.html', {'form': form})


def hair_update(request, pk):
    hair = get_object_or_404(Hair, pk=pk)
    if request.method == "POST":
        form = HairForm(request.POST, request.FILES,  instance=hair)
        if form.is_valid():
            hair = form.save(commit=False)
            hair.save()
            return redirect('hair')
    else:
        form = HairForm(instance=hair)
    return render(request, 'salonapp/hair_update.html', {'form': form})


def hair_delete(request, pk):
    salonapp = get_object_or_404(Hair, pk=pk)
    salonapp.delete()
    return redirect('hair')


def cosmet(request):
    cosmets = Cosmet.objects.all() 
    return render(request, 'salonapp/cosmet.html', {'cosmets':cosmets})


def cosmet_create(request):
    if request.method == "POST":
        form = CosmetForm(request.POST, request.FILES)
        if form.is_valid():
            cosmet = form.save(commit=False)
            cosmet.save()
            return redirect('cosmet')
    else:
        form = CosmetForm()
    return render(request, 'salonapp/cosmet_create.html', {'form': form})


def cosmet_update(request, pk):
    cosmet = get_object_or_404(Cosmet, pk=pk)
    if request.method == "POST":
        form = CosmetForm(request.POST, request.FILES,  instance=cosmet)
        if form.is_valid():
            cosmet= form.save(commit=False)
            cosmet.save()
            return redirect('cosmet')
    else:
        form = CosmetForm(instance=cosmet)
    return render(request, 'salonapp/cosmet_update.html', {'form': form})


def cosmet_delete(request, pk):
    salonapp = get_object_or_404(Cosmet, pk=pk)
    salonapp.delete()
    return redirect('cosmet')


def makeup(request):
    makeups = Makeup.objects.all() 
    return render(request, 'salonapp/makeup.html', {'makeups':makeups})


def makeup_create(request):
    if request.method == "POST":
        form = MakeupForm(request.POST, request.FILES)
        if form.is_valid():
            makeup = form.save(commit=False)
            makeup.save()
            return redirect('makeup')
    else:
        form = MakeupForm()
    return render(request, 'salonapp/makeup_create.html', {'form': form})


def makeup_update(request, pk):
    makeup = get_object_or_404(Makeup, pk=pk)
    if request.method == "POST":
        form = MakeupForm(request.POST, request.FILES,  instance=makeup)
        if form.is_valid():
            makeup= form.save(commit=False)
            makeup.save()
            return redirect('makeup')
    else:
        form = MakeupForm(instance=makeup)
    return render(request, 'salonapp/makeup_update.html', {'form': form})


def makeup_delete(request, pk):
    salonapp = get_object_or_404(Makeup, pk=pk)
    salonapp.delete()
    return redirect('makeup')


def nail(request):
    nails = Nail.objects.all() 
    return render(request, 'salonapp/nail.html', {'nails':nails})


def nail_create(request):
    if request.method == "POST":
        form = NailForm(request.POST, request.FILES)
        if form.is_valid():
            nail = form.save(commit=False)
            nail.save()
            return redirect('nail')
    else:
        form = NailForm()
    return render(request, 'salonapp/nail_create.html', {'form': form})


def nail_update(request, pk):
    nail = get_object_or_404(Nail, pk=pk)
    if request.method == "POST":
        form = NailForm(request.POST, request.FILES,  instance=nail)
        if form.is_valid():
            nail = form.save(commit=False)
            nail.save()
            return redirect('nail')
    else:
        form = NailForm(instance=nail)
    return render(request, 'salonapp/nail_update.html', {'form': form})


def nail_delete(request, pk):
    salonapp = get_object_or_404(Nail, pk=pk)
    salonapp.delete()
    return redirect('nail')

