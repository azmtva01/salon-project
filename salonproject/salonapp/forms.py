from django import forms
from .models import Lady, Portfolio, Hair, Cosmet, Makeup, Nail


class LadyForm(forms.ModelForm):

    class Meta:
        model = Lady
        fields = ('title', 'text', 'price', 'image', 'category')


class PortfolioForm(forms.ModelForm):

    class Meta:
        model = Portfolio
        fields = ('image',)


class HairForm(forms.ModelForm):

    class Meta:
        model = Hair
        fields = ('image', 'title', 'text',)


class CosmetForm(forms.ModelForm):

    class Meta:
        model = Cosmet
        fields = ('image', 'title', 'text',)        


class MakeupForm(forms.ModelForm):

    class Meta:
        model = Makeup
        fields = ('image', 'title', 'text',)


class NailForm(forms.ModelForm):

    class Meta:
        model = Nail
        fields = ('image', 'title', 'text',)
