from django.apps import AppConfig


class SalonappConfig(AppConfig):
    name = 'salonapp'
