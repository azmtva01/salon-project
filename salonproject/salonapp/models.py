from django.db import models


class Category(models.Model):
    title = models.CharField(verbose_name='Название категории ', max_length=64, blank=True, null=True)

    class Meta:
        verbose_name = "Category"    
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.title


class Lady(models.Model):
    title = models.CharField(verbose_name='Название', max_length=64, blank=True, null=True)
    text = models.TextField(verbose_name='Описание текста', blank=True, null=True)
    price = models.PositiveSmallIntegerField(verbose_name='Цена', blank=True, null=True)
    image = models.ImageField(verbose_name='Images', upload_to='images/', blank=True, null=True)
    category = models.ForeignKey(Category, verbose_name='Категория', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = "Lady"    
        verbose_name_plural = "Ladies"

    def __str__(self):
        return self.title


class Portfolio(models.Model):
    title = models.CharField(verbose_name='Название', max_length=64, blank=True, null=True)
    image = models.ImageField(verbose_name='Images', upload_to='images/', blank=True, null=True)


class Hair(models.Model):
    image = models.ImageField(verbose_name='Images', upload_to='images/', blank=True, null=True)
    title = models.CharField(verbose_name='Название', max_length=64, blank=True, null=True)
    text = models.TextField(verbose_name='Описание текста', blank=True, null=True)

    class Meta:
        verbose_name = "Hair"    
        verbose_name_plural = "Hairs"

    def __str__(self):
        return self.title        


class Cosmet(models.Model):
    image = models.ImageField(verbose_name='Images', upload_to='images/', blank=True, null=True)
    title = models.CharField(verbose_name='Название', max_length=64, blank=True, null=True)
    text = models.TextField(verbose_name='Описание текста', blank=True, null=True)

    class Meta:
        verbose_name = "Cosmet"    
        verbose_name_plural = "Cosmets"

    def __str__(self):
        return self.title


class Makeup(models.Model):
    image = models.ImageField(verbose_name='Images', upload_to='images/', blank=True, null=True)
    title = models.CharField(verbose_name='Название', max_length=64, blank=True, null=True)
    text = models.TextField(verbose_name='Описание текста', blank=True, null=True)

    class Meta:
        verbose_name = "Makeup"    
        verbose_name_plural = "Makeup"

    def __str__(self):
        return self.title        


class Nail(models.Model):
    image = models.ImageField(verbose_name='Images', upload_to='images/', blank=True, null=True)
    title = models.CharField(verbose_name='Название', max_length=64, blank=True, null=True)
    text = models.TextField(verbose_name='Описание текста', blank=True, null=True)

    class Meta:
        verbose_name = "Nail"    
        verbose_name_plural = "Nails"

    def __str__(self):
        return self.title
